FROM python:3.9

WORKDIR /code

RUN pip install pipenv

COPY . /code/

RUN pipenv install --system --deploy --ignore-pipfile

CMD ["uvicorn", "main:app", "--host", "0.0.0.0", "--port", "9000"]
